# Factory Design Pattern

## Intent

Change class's behaviour without changing its structure, so the interface that the outside world is using to interact with the class stays the same.

## Also Known As

None. Don't confuse the Factory pattern with Factoty Method or Abstract Factory patterns. These are pretty similar patterns, but they don't work interchangeably with one another.

## Motivation

The sole purpose of this design pattern is to avoid coupling the code implementation to a specific class. This pattern was developed so that the code implementation doesn't rely strictly on how one class is implemented. Rather it relies on an object returned from a general, "factory" class. This way we can easily change with which implementation of certain class our code works with changing the object returned from the factory class. The factory class must provide a promise that it will always return an object following well-defined interface so that no breaking points are introduced in our code when the object that the factory returns is changed. This is enforced using interfaces.

## Aplicability

Use the Factory design pattern when

- you want to decouple code implementation form specific class' logic;

- you don't know beforehand the exact object types that your code will work with;

- you want to provide an easy way for others to extend your code functionality with implementing classes that will work with the Factory class;


## Structure

![Factory Structure](factory.png)

## Participants

- Product
  - an interface that all concrete classes which objects will be returned through the Factory need to implement

- ConcreteProduct
  - Objects that the factory returns are often called products. This is the class that implements the Product interface and which is returned by the factory method.

- Concrete Creator
  - Concrete cretor. This is the factory class. It can extend the Creator class (if it exists, since it is optional). This is the class that decides which object (prodcut) needs to be created, instantiates the correct class and returns the code back to to client.

- Creator
  - This is an optional class that serves the purpose of adding just a bit more clarification of how the factory should work. It is crucial only if we plan to use completely different factories in our code interchangeably.

## Consequences

The Factory pattern has the following consequences:

1. The only consequence usage of this pattern introduces is that the code can become more complicated as we are introducing new subclass for each different functionality the factory needs to offer. No other consequents are directly related to this design pattern.

## Implementation

Follow these steps to successfully implement this design pattern in any language that supports OOP paradigm.

1. Create an interface containg a method that each product class need to implement so it can be used by the cient code without regards to which class was instantiated.

2. Create a class (or couple of class) implementing that interface.

3. Create a factory class which has a method that decides which object that implements the interface should be instantiated and returned back to the client.

4. (Optional) Create abstract class or interface from which the factory class will inherit or implement if you want to enable option to swtich between different factories during the lifetime of the project.

Here's is a pseudo code representation of the Factory design pattern.


    ```
    class Creator {
        private function getProduct(variable) {
            if(variable == 'foo') {
                return new Product1()
            } else if(variable == 'bar') {
                return new Product2()
            }
        }
    }

    interface Product {
        private function doSomeStuff();
    }

    class Product1 implements Product{
        private function doSomeStuff() {
            print 'Hi from Product 1';
        }
    }

    class Product2 implements Product{
        private function doSomeStuff() {
            print 'Hi from Product 2';
        }
    }
    ```

Then when we want to use the factory, we can use both call the doSomeOperation method, and regarding to the variable passed it either insantiates Product1 or Product2 and then we actually call their methods

    ```
    product1 = (new Factory())->getProduct('foo')
    product->doSomeStuff() //prints: Hi from Product 1

    product2 = (new Factory())->getProduct('bar')
    product->doSomeStuff(); //prints: Hi from Product 2
    ```

## Example

In the php-example there is a sample implementation of this pattern with instantiating and using different kind of users that should perform the same action.