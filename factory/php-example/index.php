<?php

// Factory design pattern

// The factory class that returns different kind of 
// objects which implement the same interface
class Factory
{
    public function getUser($type)
    {
        if (strtolower($type) == 'student') {
            return new Student();
        } else if (strtolower($type) == 'mentor') {
            return new Mentor();
        }
    }
}

// The interface that each product class needs to implement
// so that it can be used interchangeable by the code logic
// in the factory class and the client code
interface Person
{
    public function attendClass();
}

// Concrete implementaion of one type of product
class Student implements Person
{
    public function attendClass()
    {
        echo "I am a student and I learn";
    }
}

// Concrete implementaion of another type of product
class Mentor implements Person
{
    public function attendClass()
    {
        echo "I am a mentor and I teach";
    }
}

// Usage:
// We instantiate a factory class
$factory = new Factory();

// Then, whenever we need a certain type of product
// we delegate the code to the factory's method 
// responsible for creating objects
$student = $factory->getUser("student");

$mentor = $factory->getUser("MENTOR");

// Each product's logic behaves differently even though
// the process of their creating was the same
$student->attendClass(); // Prints: I am a student and I learn
echo "<hr />";
$mentor->attendClass(); // Prints: I am a mentor and I teach