<?php

// Decorator design pattern

// The intreface that both the Component and the Decorator will implement so they can be used interchangeably
interface Printable
{
    public function getText();
}


// This is a class that simulates data being fetched from any source. 
// The data will be stored in a private property for demonstration purposes.
// We can use it as it is or we can first wrap the object into a decorator and then use its methods
class SourceText implements Printable
{
    private $text = "Lorem ipsum <b>dolor</b> sit amet";

    public function getText()
    {
        return $this->text;
    }
}

// This class can actually be ommited, but it is nice to have a starting point 
// that all decorators following this interface will extend. This way it provides a
// safe way that even if some of the concrete decorators fails to implement the 
// interface method, the code would still work
class Decorator implements Printable
{
    private $object;

    public function __construct(Printable $object)
    {
        $this->object = $object;
    }

    public function getText()
    {
        return $this->object->getText();
    }
}

// Concrete decorator 1 (makes all the letters uppercase)
class UppercaseDecorator extends Decorator
{
    public function getText()
    {
        return strtoupper(parent::getText());
    }
}

// Concrete decorator 2 (strips html tags by changing < with &lt; and > with &gt;)
class StripHtmlTagsDecorator extends Decorator
{
    public function getText()
    {
        return htmlspecialchars(parent::getText());
    }
}

// Usage:
// The original class
$sourceText = new SourceText();

// Objects from this class can be used right away, undecorated
echo $sourceText->getText(); //prints: Lorem ipsum <b>dolor</b> sit amet

echo "<hr />";

// However, some objects can be wrapped into a Decorator class which changes the original behaviour.
// Decorated behavior: 
$uppercaseDecorator = new UppercaseDecorator($sourceText);
echo $uppercaseDecorator->getText(); //prints: LOREM IPSUM <b>DOLOR</b> SIT AMET

echo "<hr />";

// This pattern offers having mutliple layers of decorating. The previously decorated object with UppercaseDecorator
// can be decorated again and again and again. Below we are decorating it so that hmtl tags are replaced
// with html entities
// Decorated behaviour:
$stripHTMLDecorator = new StripHtmlTagsDecorator($uppercaseDecorator);
echo $stripHTMLDecorator->getText(); //prints: LOREM IPSUM &lt;b&gt;DOLOR&lt;/b&gt; SIT AMET
