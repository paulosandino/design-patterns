# Decorator Design Pattern

## Intent

Change class's behaviour without changing its structure, so the interface that the outside world is using to interact with the class stays the same.

## Also Known As

Wrapper

## Motivation

Just as the alias name suggest, this patterns should be used when we want to change how a given class' methods are behaving without changing the class itself and in situations where inheritance is not an option. This way, besides some programming languages don't support multiple inheritance, they can achieve all the functionality they would have if they supported it. It's also sometimes related to as "composition over inheritance". This pattern moreover enables adding new functionalities to specific object of certain classes, without tampering the class itself and so leaving all the other objects untouched.

## Aplicability

Use the Decorator pattern when

- you want to add specific functionalities only to some objects of a certain class, not all of them;

- you want to remove specific functionalities from some objects of a certain class;

- you want to avoid too much sub-classing to add additional functionalities;


## Structure

![Decorator Structure](decorator.png)

## Participants

- Component
  - an ordinary class from which we can instantiate objects which will be decorated later.

- ConcreteComponent
  - an object from the Component class. It can be used right away, or it can first be decorated and then used.

- Decorator
  - the class that extends the functionality of the Component class. It wraps the ConcreteComponent object's functionality and provides a blueprint for further decorating.

- ConcreteDecorator
  - concrete decorator classes which actually do all the decorating after which they call the ordinary component's method

## Consequences

The Decorator pattern has the following consequences:

1. Decorator pattern enables adding specific functionalities to objects from the same. Depending on how is it used, this behaviour can violate the basic advantages that OOP offers, mainly the act of polymorphism. Since there are objects from the same class that behave differently, interchaning objects and coding to interface can become cumbersome.

2. It can be hard to grasp where each functionality comes from, since this pattern often works on multiple levels, meaning more decorator components are aplied to single object like tiers, each making its own changes.

## Implementation

Follow these steps to successfully implement this design pattern in any language that supports OOP paradigm.

1. Create an interface containg a method that will later be decorated.

2. Create a class implementing that interface.

3. Create a decorator class which has a constructor that accepts an object implementing the interface from above and assigns this object as a value to its own property.

4. Create a class that extends the decorator class and implements the interface from above. The method that is implemented regarding the interface does some functionality (the decorated one) and then calls the same method on the component object so that the base functionality is executed as well.

Here's is a pseudo code representation of the Decorator design pattern.


    ```
    interface iDoSomething {
        private function do();
    }

    class Component implements iDoSomething{
        private function do() {
            print 'Hi';
        }
    }

    class Decorator {
        public object = null

         private function constructor(iDoSomething component) {
            this->object = component
        }

        private function do() {
            object->do();
        }
    }

    class RealDecorator exteds Decorator implements iDoSomething {
        private function do() {
            print "Oy, "
            parent::do()
        }
    }
    ```

Then when we want to initialize this class, we can use both undecorated and decorated versions: 

    ```
    component = new Component()
    component->do() //prints: Hi

    decorator = new RealDecorator(component)
    decorator->do(); //prints: Oy, Hi
    ```

## Example

In the php-example there is a sample implementation of this class taking content from some source (api, scrapping, database, userinput etc.), making it all uppercase and removing all the HTML tags.