# Bridge Design Pattern

## Intent

Prevent a class from having more than one instance at the same time.

## Motivation

Some classes are expensive to instantiate. Their constructors perform some processing-heavy operations like generating historic data or perform some time-consuming operations such as connecting to third party APIs or other applications like database servers. In such cases, we want to avoid having multiple instances where possible. Singleton design pattern enforces that only one instance of a class is kept at any given moment and all the remaining code interacts with that sole instance.

## Aplicability

Use the Singleton pattern when

- only one instance of a class should exists;

- all the remaining code interacting with the instance shares the same data or in other words the instance serves a general purpose for all the remaining code logic;

- you want to ensure that expensive (time and processing) operations are run only once when the Singleton class is insantiated.

## Structure

![Bridge Structure](singleton.png)

## Participants

- Singleton
  - stores a reference to the single instance that should exist;
  - offers a method that initializes the singleton instance if it doesn't exists or returns it otherwise.

## Consequences

The Singleton pattern has the following consequences:

1. Critics consider the singleton to be an anti-pattern in that it is frequently used in scenarios where it is not beneficial because it often introduces unnecessary restrictions in situations where a singleton class would not be beneficial, thereby introducing global state into an application.

2. Because a singleton can be accessed anywhere by anything without having to use any parameters, any dependencies would not be immediately visible to developers. Consequently, developers would need to know "the inner workings of code to properly use it".

3. Singletons also violate the single-responsibility principle, because not only are they responsible for the singleton's normal task, it must also ensure that only one is instantiated; 

4. Singletons are difficult to test because they carry global state for the duration of the program. Specifically, because unit testing requires loosely coupled classes in order to isolate what's being tested. Additionally, when a certain class interacts with a singleton, that class and the singleton become tightly coupled, making it impossible to test the class on its own without also testing the singleton.

## Implementation

The Singleton is one of the easiest design patters to understand and implement. Follow these steps to successfully implement this design patters in any language that supports OOP paradigm.

1. Create a class with a single static property, often named instance. This property will contain the single instance that will exists during the lifecycle of the request.

2. Create a private constructor method that performs all the operations that lead to this class being a fit for Singleton design pattern.

3. Create a public static method which will return the value of the static property (hence the instance of the class). This method should check if the property is set to an instance and return it, otherwise call the constructor which will return an instance from the class, then init the class' property with this instance and return it.  

Here's is a pseudo code representation of the Singleton design pattern.


    ```
    Class Foo {
        public static instance = null

        private function constructor() {
            //...perform whatever expensive operation lead to this implementaion
        }

        public function getInstance() {
            if(instance == null) {
                instance = new self();
            }

            return instance;
        }
    }
    ```

Then when we want to initialize this class, we don't call the controller, but the static `get` method, like so: 

    ```
    instance = Foo::getInstance()
    ```

## Example

In the php-example there is a sample implementation of this class for connecting to a database using the PDO object for connecting. Multiple calls to the same class don't open new connections to the database but rather open just one during the first call and then reuse that one each time we call the connect method again.