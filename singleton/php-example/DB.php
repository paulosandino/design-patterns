<?php

///Example class that connects to database. 
// We don't want more than one connection at any given time so this is a perfect example to rely on the 
// Singleton design pattern

class DB
{
    // The property that keeps track whether we already have an instance or not
    // By default null, meaning intially we don't have insance
    private static $instance = null;

    // The constructor that does the "expensive" operation
    private function __construct()
    {
        self::$instance = new PDO("mysql:dbnname=NAME;host=localhost", "username", "password");
    }

    // The `getInstance()` method that actually does the magic
    // We never call the constructor manually, rather this static funtion that then calls the consturctor if necessary
    public static function connect()
    {
        if (is_null(self::$instance)) {
            new self();
        }

        return self::$instance;
    }
}

// Instantiation. If we want to connect to database, we would use the above class like this:

$db = DB::connect();

// Even if we call it multiple times, it will still connect only once (the first time and return the existing instance in the following attemts)

$db = DB::connect(); //just returning the instance created previously
$db = DB::connect(); //just returning the instance created previously
$db = DB::connect(); //just returning the instance created previously