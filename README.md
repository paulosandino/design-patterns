# Design Patterns

The main goal of this repository is to aggregate explanation and examples of differents design patterns found in the literature.

Anyone can contribute to this repository, you only need to fork the repository, add an example of the design patters and a description about it.
You can follow the previously accepted models of description.
You can also add examples in different languages if you want to.

## Creational Patterns
- [Singleton Pattern](https://gitlab.com/paulosandino/design-patterns/-/tree/master/singleton)
- [Factory Pattern](https://gitlab.com/paulosandino/design-patterns/-/tree/master/factory)

## Structural Patterns
- [Adapter Pattern](https://gitlab.com/paulosandino/design-patterns/-/tree/master/adapter)
- [Bridge Pattern](https://gitlab.com/paulosandino/design-patterns/-/tree/master/bridge)
- [Decorator Pattern](https://gitlab.com/paulosandino/design-patterns/-/tree/master/decorator)

## Behavioral Patterns
